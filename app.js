/**
 * External Dependencies
 *
 * @format
 */
const express = require( 'express' );

/**
 *  This `app` is the central part of our application.
 *  We'll extend it as we go along by adding routes,
 *  middleware and other settings
 */
const app = express();

/**
 * Array for Loops
 */
// const colors = [ 'red', 'orange', 'yellow', 'green', 'blue', 'purple' ];

/**
 * Set the view engine for application
 */
app.set( 'view engine', 'pug' );

/**
 *  The `get()` method is used to handle
 *  the GET requests to a certain URL.
 */
app.get( '/', ( req, res ) => {
	res.render( 'index' );
} );

app.get( '/cards', ( req, res ) => {
	// res.send( '<h1>Bonjour, JavaScript Engineer!</h1>' );

	// Set variables to `locals` via second optional param object:
	res.render( 'card', {
		cardPrompt: "Who is buried in Grant's tomb?",
		promptHint: "Think about who's tomb it is ... dummy!",
		// colors,
	} );

	// Alternate syntax to setting variables via `locals`:
	// res.locals.cardPrompt = "Who is buried in Grant's tomb?";
	// res.render( 'card' );
} );

app.get( '/hello', ( req, res ) => {
	res.render( 'hello' );
} );

/**
 *  The `post()` method is used to handle
 *  the POST requests from a certain URL.
 */
app.post( '/hello', ( req, res ) => {
	res.render( 'hello' );
} );

/**
 *  Set up the dev server via the `listen method`
 *  Accepts port number
 */
app.listen( 3000, () => {
	console.log( 'Your application is running on http://localhost://3000' );
} );
